/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import antlr.TokenStreamException;

/**
 * This exception is thrown when a parsing error
 * occurs.
 * <p/>
 * The functionality is basically provided by an ANTLR
 * Lexer, which still does not allow custom throws in
 * production rules. Therefor this is a <tt>RuntimeException</tt>
 * that requires to be catched when calling {@link BBCodeEventParser#parse()}.
 * </p>
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class BBCodeException 
  extends TokenStreamException {

  private int m_Line = -1;

  /**
   * Constructs a new <tt>BBCodeException</tt> with a message.
   *
   * @param msg the message.
   */
  public BBCodeException(String msg) {
    super(msg);
  }//BBCodeException

  /**
   * Constructs a new <tt>BBCodeException</tt> with a message.
   *
   * @param msg  the message.
   * @param line the line number of the input that caused the exception.
   */
  public BBCodeException(String msg, int line) {
    super(msg);
    m_Line = line;
  }//BBCodeException

  /**
   * Returns the number of the line that caused the exception.
   *
   * @return the current line number as <tt>int</tt>.
   */
  public int getLine() {
    return m_Line;
  }//getLine

}//BBCodeException