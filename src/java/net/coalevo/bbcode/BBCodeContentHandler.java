/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

/**
 * Defines the contract for a BBCode fragment or document
 * content handler.
 * 
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface BBCodeContentHandler {

  /**
   * Called when a start element has been parsed from the source.
   * 
   * @param name the name of the element.
   * @param attr a possible attribute of the element.
   * @throws BBCodeException if the implementation did not expect that element.
   */
  public void startElement(String name, String attr) 
      throws BBCodeException;
  
  /**
   * Called when an end element has been parsed from the source.
   * 
   * @param name the name of the element.
   * @throws BBCodeException if the implementation did not expect that element.
   */
  public void endElement(String name) throws BBCodeException;
  
  /**
   * Called when characters have been parsed from the source.
   * <p>
   * Note that all linebreaks will be converted to a single newline character.
   * If linebreaks or whitespace needs to filtered, the parameter content has
   * to be processed accordingly. This is required because there might be
   * element content that allows whitespace and linebreaks (e.g. <tt>pre</tt> or <tt>code</tt>).
   * </p>
   *
   * @param chars the parsed characters.
   * @throws BBCodeException if the implementation did not expect characters,
   *         or does not allow certain characters.
   */
  public void characters(String chars) throws BBCodeException;

  /**
   * Called when newline characters appear in the source.
   * <p>
   * Note that all linebreaks will be converted to a single newline character.
   * The handler might wish to interpret these according to its state.
   * </p>
   * @throws BBCodeException if the implementation did not expect characters,
   *         or does not allow certain characters.
   */
   public void newline() throws BBCodeException;
   
   /**
    * Called when whitespace appears in the source.
    * <p>
    * Whitespace is separated due to the fact that the handler might want 
    * to interpret it according to its context.
    * </p>
    * @param chars the parsed whitespace characters.
    * @throws BBCodeException if the implementation did not expect characters,
    *         or does not allow certain characters.
    */
   public void whitespace(String chars) throws BBCodeException;
   
   /**
    * Called when preformatted text appears in the source.
    * <p>
    * This will pass through the text as is, so maybe linebreaks need to be
    * translated.
    * </p>
    * @param chars the parsed whitespace characters.
    * @throws BBCodeException if the implementation did not expect characters,
    *         or does not allow certain characters.
    */
   public void preformatted(String chars) throws BBCodeException;

}//interface BBCodeContentHandler
