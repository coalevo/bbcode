/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import net.coalevo.text.util.MarkupLanguageFilter;
import net.coalevo.text.util.ANSIHelper;

import java.io.*;
import java.util.*;

/**
 * Provides a BBCode to ANSI transformer.
 * <p/>
 * Note: for the telnetd, it is not recommendable
 * to use this, rather it should be implemented with internal
 * markups, so that terminals without color support do not
 * receive ESCAPE sequences they cannot interpret.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BBCode2ANSI
    extends BaseContentHandler {

  private Writer m_Out;
  private ANSIHelper m_Ansi = new ANSIHelper();
  private Stack m_List;
  private int m_HeaderLength = -1;
  private Table m_Table;
  private StringBuilder m_Cell;

  private BBCode2ANSI(Reader in, Writer out) {
    m_Parser = BBCodeEventParser.create(in, this);
    m_Out = out;
    m_List = new Stack();
  }//contructor

  public void transform() throws BBCodeException {
    try {
      m_Parser.parse();
    } catch (Exception ex) {
      throw new BBCodeException("Syntax error:" + ex.getMessage(), m_Parser.getCurrentLine());
    }
  }//transform

  public void startBold() {
    write(m_Ansi.startStyle(ANSIHelper.BOLD));
  }//startBold

  public void endBold() {
    write(m_Ansi.endStyle());
  }//endBold

  public void startUnderline() {
    write(m_Ansi.startStyle(ANSIHelper.UNDERLINED));
  }

  public void endUnderline() {
    write(m_Ansi.endStyle());
  }//endUnderline

  public void startItalic() {
    write(m_Ansi.startStyle(ANSIHelper.ITALIC));
  }//startItalic

  public void endItalic() {
    write(m_Ansi.endStyle());
  }//endItalic

  public void startColor(String color) {
    write(m_Ansi.startFgColor(ANSIHelper.getColor(color)));
  }//startColor

  public void endColor() {
    write(m_Ansi.endFgColor());
  }//endColor

  public void startStyle(String style) {
  }//startStyle

  public void endStyle() {
  }//endStyle

  public void startSize(int size) {

  }//startSize

  public void endSize() {
  }//endSize

  public void startAlphanumericList() {
    appendLineBreak();
    m_List.push(new AlphanumericList());
  }//startAlphanumeric

  public void endAlphanumericList() {
    if (!m_List.isEmpty()) {
      m_List.pop();
    }
    appendLineBreak();
  }//endAlphanumericList

  public void startNumericList() {
    appendLineBreak();
    m_List.push(new NumericList());
  }//startNumericList

  public void endNumericList() {
    if (!m_List.isEmpty()) {
      m_List.pop();
    }
    appendLineBreak();
  }//endNumericList

  public void startBulletedList() {
    appendLineBreak();
    m_List.push(new BulletList());
  }//startBulletedList

  public void endBulletedList() {
    if (!m_List.isEmpty()) {
      m_List.pop();
    }
    appendLineBreak();
  }//endBulletedList

  public void startListItem() {
    if (!m_List.isEmpty()) {
      write(((AList) m_List.peek()).next());
    }
  }//startListItem

  public void endListItem() {
    appendLineBreak();
  }//endListItem

  public void appendLink(String ref, String name) {
    write(" ");
    write((name.equals(ref)) ? "" : name);
    startColor("green");
    write(" ==> ");
    endColor();
    startColor("cyan");
    write(ref);
    endColor();
    write(" ");
  }//appendLink

  public void appendImage(String src, String alt) {
    write(" [Image:\"");
    write(alt);
    write("\", ");
    startColor("cyan");
    write(src);
    endColor();
    write("] ");
  }//appendImage

  public String formatImage(String src, String alt) {
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append("[Image:\"")
        .append(alt)
        .append("\"]");
    return sbuf.toString();
  }//formatImage

  public void appendContent(String str) {
    if(m_HeaderLength >= 0) {
      m_HeaderLength += str.length();
    }
    if(m_Cell != null) {
      m_Cell.append(str);
      return;
    }
    write(str);
  }//appendContent

  public void appendCode(String str, String type) {
    write("\n\n-- Code");
    write((type == null) ? "" : " (" + type + ")");
    write(" --\n");
    write(str);
    write("\n-- End Code --\n\n");
  }//appendCode

  public void appendLineBreak() {
    write(LINEBREAK);
  }//appendLineBReak

  public void startHeader(int i) {
     m_HeaderLength = 0;
     switch (i) {
       case 1:
       case 2:
       case 3:
       case 4:
       default:
         startBold();
         startColor("white");
     }
   }//startHeader

  public void endHeader(int i) {
    switch (i) {
      case 1:
        endColor();
        appendLineBreak();
        startColor("red");
        write(getString('=', m_HeaderLength));
        endColor();
        endBold();
        appendLineBreak();
        break;
      case 2:
        endColor();
        appendLineBreak();
        startColor("green");
        write(getString('=', m_HeaderLength));
        endColor();
        endBold();
        appendLineBreak();
        break;
      case 3:
        endColor();
        this.appendLineBreak();
        startColor("yellow");
        write(getString('-', m_HeaderLength));
        endColor();
        endBold();
        appendLineBreak();
        break;
      case 4:
        appendLineBreak();
        write(getString('-', m_HeaderLength));
        endColor();
        endBold();
        appendLineBreak();
        break;
      default:
        
    }
    m_HeaderLength = -1;
  }//startHeader

  public void startTable() {
    m_Table = new Table();
  }//begin_table

  public void endTable() {
    //printout
    write(m_Table.toString());
    m_Table = null;
  }//endTable

  public void startTableCell() {
    m_Cell = new StringBuilder();
  }//startCell

  public void endTableCell() {
    m_Table.addCell(m_Cell.toString());
    m_Cell =null;
  }//endCell

  public void startTableRow() {
    m_Table.nextRow();
  }//startRow

  public void endTableRow() {
  }//endRow

  public void startBlockquote() {
    write("\n\n-- Quote --\n");
    write(m_Ansi.startStyle(ANSIHelper.BOLD));
  }//startBlockquote

  public void endBlockquote() {
    write(m_Ansi.endStyle());
    write("\n-- End Quote --\n\n");
  }//endBlockquote

  private void write(String str) {
    try {
      m_Out.write(str);
    } catch (IOException ex) {
    }
  }//write

  public static void transform(Reader in, Writer out) throws BBCodeException {
    BBCode2ANSI tf = new BBCode2ANSI(in, out);
    tf.transform();
  }//transform

  private static abstract class AList {

    public abstract String next();

  }//AList

  private static class NumericList extends AList {

    int m_Num = 1;

    public String next() {
      return m_Num++ + ". ";
    }//next
  }//NumericList

  private static class AlphanumericList extends AList {

    char m_Alpha = 'a';

    public String next() {
      return m_Alpha++ + ". ";
    }//next

  }//AlphanumericList

  private static class BulletList extends AList {

    public String next() {
      return "* ";
    }
  }//BulletList


  public static void main(String[] args) {
    try {
      StringWriter sw = new StringWriter();
      BBCode2ANSI.transform(new FileReader(args[0]), sw);
      sw.flush();
      System.out.println(sw.toString());
      /*
      AlphanumericList l = new AlphanumericList();
      System.out.println(l.next());
      System.out.println(l.next());
      System.out.println(l.next());
      System.out.println(l.next());
      System.out.println(l.next());
      System.out.println(l.next());
      NumericList li = new NumericList();
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      System.out.println(li.next());
      */
    } catch (BBCodeException bbex) {
      System.out.println("BBCode error on line " + bbex.getLine() + "::" + bbex.getMessage());
      bbex.printStackTrace();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//main

    private static class Table {

    private int m_RIdx;
    private List<List<String>> m_Rows;
    private Map<Integer, Integer> m_ColSizes;

    public Table() {
      m_ColSizes = new HashMap<Integer, Integer>();
      m_Rows = new ArrayList<List<String>>();
      m_RIdx = -1;
    }//constructor

    public void addCell(String str) {
      str = MarkupLanguageFilter.filterMLWS(str);
      List<String> cols = m_Rows.get(m_RIdx);

      int colidx = cols.size();
      int collen = ANSIHelper.getVisibleLength(str);

      if (m_ColSizes.containsKey(colidx)) {
        int ll = m_ColSizes.get(colidx);
        if (collen > ll) {
          m_ColSizes.put(colidx, collen);
        }
      } else {
        m_ColSizes.put(colidx, collen);
      }
      cols.add(str);
    }//addCell

    public void nextRow() {
      m_Rows.add(new ArrayList<String>());
      m_RIdx++;
    }//nextRow

    public String toString() {
      StringBuilder sb = new StringBuilder();
      //1. Prepare table top and bottom
      StringBuilder tabletop = new StringBuilder();
      StringBuilder tablebot = new StringBuilder();
      tabletop.append(LEFT_UPPER);
      tablebot.append(LEFT_LOWER);
      for (int i = 0; i < m_ColSizes.size(); i++) {
        int width = m_ColSizes.get(i)+1;
        tabletop.append(getString(HORIZONTAL, width + 1));
        tablebot.append(getString(HORIZONTAL, width + 1));
        if (i < m_ColSizes.size() - 1) {
          tabletop.append(RIGHT_UPPER);
          tablebot.append(RIGHT_LOWER);
        } else {
          tabletop.append(MID_UPPER);
          tablebot.append(MID_LOWER);
        }
      }

      //2. Add table top
      sb.append(LINEBREAK);
      sb.append(tabletop.toString());
      sb.append(LINEBREAK);

      //3. Add rows
      for (int n = 0; n < m_Rows.size(); n++) {
        List<String> cols = m_Rows.get(n);
        boolean islastrow = (n == m_Rows.size() - 1);

        StringBuilder row = new StringBuilder();
        StringBuilder rowsep = new StringBuilder();

        int numcols = cols.size();
        for (int i = 0; i < numcols; i++) {
          int width = m_ColSizes.get(i)+1;
          String colstr = cols.get(i);
          if (i == 0) {
            rowsep.append(LEFT_MID);
            rowsep.append(getString(HORIZONTAL, width + 1));
            //Row first with a vertical bar
            row.append(VERTICAL);
          } else {
            rowsep.append(MID_MID);
            rowsep.append(getString(HORIZONTAL, width + 1));
          }
          if (i == (numcols - 1)) {
            rowsep.append(RIGHT_MID);
          }

          appendPadded(colstr, width, row);
          row.append(VERTICAL);
        }
        //Add row
        sb.append(row.toString());
        sb.append(LINEBREAK);
        //Add Separator
        if (!islastrow) {
          sb.append(rowsep);
          sb.append(LINEBREAK);
        }

      }

      //4. Add table bottom
      sb.append(tablebot.toString());
      sb.append(LINEBREAK);

      return sb.toString();
    }//toString

    public void appendPadded(String str, int width, StringBuilder sbuf) {
      int len = ANSIHelper.getVisibleLength(str);
      sbuf.append(" ");
      sbuf.append(str);
      sbuf.append(getString(' ', width - len));
    }//appendPadded


    private static char HORIZONTAL = '-';
    private static char VERTICAL = '|';

    private static char LEFT_UPPER = '+';
    private static char RIGHT_UPPER = '+';
    private static char MID_UPPER = '+';

    private static char LEFT_MID = '+';
    private static char RIGHT_MID = '+';
    private static char MID_MID = '+';

    private static char LEFT_LOWER = '+';
    private static char RIGHT_LOWER = '+';
    private static char MID_LOWER = '+';

    /* Extended ASCII Set
    private static char HORIZONTAL = (char)196;
    private static char VERTICAL = (char)179;

    private static char LEFT_UPPER = (char)218;
    private static char RIGHT_UPPER = (char)191;
    private static char MID_UPPER = (char)194;

    private static char LEFT_MID = (char)195;
    private static char RIGHT_MID = (char)180;
    private static char MID_MID = (char) 197;

    private static char LEFT_LOWER = (char)192;
    private static char RIGHT_LOWER = (char)217;
    private static char MID_LOWER = (char)193;
    */

  }//Table


  private static final String getString(char c, int times) {
    final StringBuilder sbuf = new StringBuilder();
    while (times-- > 0) {
      sbuf.append(c);
    }
    return sbuf.toString();
  }//getString




  public static final String LINEBREAK = "\n";


}//class BBCode2ANSI