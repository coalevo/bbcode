/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

/**
 * Implementation of a {@link BBCodeContentHandler}
 * for debugging purposes.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class SimpleBBCodeContentHandler
    implements BBCodeContentHandler {

  public void startElement(String name, String attr) throws BBCodeException {
    System.out.println("startElement()::" + name + "::" + attr);
  }//startElement
  
  public void endElement(String name) throws BBCodeException {
    System.out.println("endElement()::" + name);
  }//endElement
  
  public void characters(String chars) throws BBCodeException {
    System.out.println("characters()::" + chars);
  }//characters

  public void newline() throws BBCodeException {
    System.out.println("newline()");
  }//newline

  public void whitespace(String chars) throws BBCodeException {
    System.out.println("whitespace()");
  }//whitespace
  
  public void preformatted(String chars) throws BBCodeException {
    System.out.println("preformatted()" + chars);
  }//preformatted

}//interface BBCodeContentHandler
