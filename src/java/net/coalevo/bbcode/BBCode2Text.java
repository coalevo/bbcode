/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import java.io.*;
import java.util.*;

/**
 * Provides a BBCode to Text transformer.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BBCode2Text
    extends BaseContentHandler {

  private Writer m_Out;
  private int m_HeaderLength = -1;

  private BBCode2Text(Reader in, Writer out) {
    m_Parser = BBCodeEventParser.create(in, this);
    m_Out = out;
  }//contructor

  public void transform() throws BBCodeException {
    try {
      m_Parser.parse();
    } catch (Exception ex) {
      throw new BBCodeException("Syntax error:" + ex.getMessage(), m_Parser.getCurrentLine());
    }
  }//transform

  public void startBold() {
  }//startBold

  public void endBold() {
  }//endBold

  public void startUnderline() {
  }//startUnderline

  public void endUnderline() {
  }//endUnderline

  public void startItalic() {
  }//startItalic

  public void endItalic() {
  }//endItalic

  public void startColor(String color) {
  }//startColor

  public void endColor() {
  }//endColor

  public void startStyle(String style) {
  }//startStyle

  public void endStyle() {
  }//endStyle

  public void startSize(int size) {

  }//startSize

  public void endSize() {
  }//endSize

  public void startAlphanumericList() {
  }//startAlphanumeric

  public void endAlphanumericList() {
  }//endAlphanumericList

  public void startNumericList() {
  }//startNumericList

  public void endNumericList() {
  }//endNumericList

  public void startBulletedList() {
  }//startBulletedList

  public void endBulletedList() {
  }//endBulletedList

  public void startListItem() {
  }//startListItem

  public void endListItem() {
  }//endListItem

  public void appendLink(String ref, String name) {
  }//appendLink

  public void appendImage(String src, String alt) {
  }//appendImage

  public String formatImage(String src, String alt) {
    return "";
  }//formatImage

  public void appendContent(String str) {
    write(str);
  }//appendContent

  public void appendCode(String str, String type) {
    //stripped
  }//appendCode

  public void appendLineBreak() {
    write(LINEBREAK);
  }//appendLineBReak

  public void startHeader(int i) {
   }//startHeader

  public void endHeader(int i) {
    appendLineBreak();
  }//startHeader

  public void startTable() {
  }//begin_table

  public void endTable() {
  }//endTable

  public void startTableCell() {
  }//startCell

  public void endTableCell() {
    write(" ");
  }//endCell

  public void startTableRow() {
  }//startRow

  public void endTableRow() {
    appendLineBreak();
  }//endRow

  public void startBlockquote() {
  }//startBlockquote

  public void endBlockquote() {
  }//endBlockquote

  private void write(String str) {
    try {
      m_Out.write(str);
    } catch (IOException ex) {
    }
  }//write

  public static void transform(Reader in, Writer out) throws BBCodeException {
    BBCode2Text tf = new BBCode2Text(in, out);
    tf.transform();
  }//transform

  public static void main(String[] args) {
    try {
      StringWriter sw = new StringWriter();
      transform(new FileReader(args[0]), sw);
      sw.flush();
      System.out.println(sw.toString());

    } catch (BBCodeException bbex) {
      System.out.println("BBCode error on line " + bbex.getLine() + "::" + bbex.getMessage());
      bbex.printStackTrace();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//main

  public static final String LINEBREAK = "\n";

}//class BBCode2Text
