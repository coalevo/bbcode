/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import java.io.*;

/**
 * Provides transformations of BBCode to different formats.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class BBCodeTransformer {

  public String toANSI(String str) throws BBCodeException {
    final StringWriter sw = new StringWriter();
    final StringReader sr = new StringReader(str);
    BBCode2ANSI.transform(sr, sw);
    sw.flush();
    return sw.toString();
  }//toANSI

  public void toANSI(Reader in, Writer out) throws BBCodeException {
    BBCode2ANSI.transform(in,out);
  }//toANSI
  
  public String toTelnetd(String str) throws BBCodeException {
    final StringWriter sw = new StringWriter();
    final StringReader sr = new StringReader(str);
    BBCode2Telnetd.transform(sr, sw);
    sw.flush();
    return sw.toString();
  }//toTelnetd

  public void toTelnetd(Reader in, Writer out) throws BBCodeException {
    BBCode2Telnetd.transform(in,out);
  }//toTelnetd
  
  public String toHTML(String str) throws BBCodeException {
    final StringWriter sw = new StringWriter();
    final StringReader sr = new StringReader(str);
    BBCode2HTML.transform(sr, sw);
    sw.flush();
    return sw.toString();
  }//toHTML

  public void toHTML(Reader in, Writer out) throws BBCodeException {
    BBCode2HTML.transform(in,out);
  }//toHTML

    public String toText(String str) throws BBCodeException {
    final StringWriter sw = new StringWriter();
    final StringReader sr = new StringReader(str);
    BBCode2Text.transform(sr, sw);
    sw.flush();
    return sw.toString();
  }//toText

  public void toText(Reader in, Writer out) throws BBCodeException {
    BBCode2Text.transform(in,out);
  }//toText

}//BBCodeTransformer