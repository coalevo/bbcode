/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import net.coalevo.text.util.MarkupLanguageFilter;

import java.io.*;

/**
 * Provides a BBCode to HTML transformer.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BBCode2HTML
    extends BaseContentHandler {

  private Writer m_Out;

  private BBCode2HTML(Reader in, Writer out) {
    m_Parser = BBCodeEventParser.create(in, this);
    m_Out = out;
  }//contructor

  public void transform() throws BBCodeException {
    try {
      m_Parser.parse();
    } catch (Exception ex) {
      throw new BBCodeException("Syntax error:" + ex.getMessage(), m_Parser.getCurrentLine());
    }
  }//transform

  public void startBold() {
    write(START_BOLD);
  }//startBold

  public void endBold() {
    write(END_BOLD);
  }//endBold

  public void startUnderline() {
    write(START_UNDERLINE);
  }

  public void endUnderline() {
    write(END_UNDERLINE);
  }//endUnderline

  public void startItalic() {
    write(START_ITALIC);
  }//startItalic

  public void endItalic() {
    write(END_ITALIC);
  }//endItalic

  public void startColor(String color) {
    write(START_COLOR_PRE + color + START_COLOR_POST);
  }//startColor

  public void endColor() {
    write(END_COLOR);
  }//endColor

  public void startStyle(String style) {
    write(START_STYLE_PRE + style + START_STYLE_POST);
  }//startStyle

  public void endStyle() {
    write(END_STYLE);
  }//endStyle

  public void startSize(int size) {
    write(START_SIZE_PRE + size + START_SIZE_POST);
  }//startSize

  public void endSize() {
    write(END_SIZE);
  }//endSize

  public void startAlphanumericList() {
    write(START_ALPHANUMERIC_LIST);
  }//startAlphanumeric

  public void endAlphanumericList() {
    write(END_ALPHANUMERIC_LIST);
  }//endAlphanumericList

  public void startNumericList() {
    write(START_NUMERIC_LIST);
  }//startNumericList

  public void endNumericList() {
    write(END_NUMERIC_LIST);
  }//endNumericList

  public void startBulletedList() {
    write(START_BULLETED_LIST);
  }//startBulletedList

  public void endBulletedList() {
    write(END_BULLETED_LIST);
  }//endBulletedList

  public void startListItem() {
    write(START_LISTITEM);
  }//startListItem

  public void endListItem() {
    write(END_LISTITEM);
  }//endListItem

  public void appendLink(String ref, String name) {
    write(START_LINK_PRE + ref + START_LINK_POST);
    write(name);
    write(END_LINK);
  }//appendLink

  public void appendImage(String src, String alt) {
    write(formatImage(src, alt));
  }//appendImage

  public String formatImage(String src, String alt) {
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(START_IMAGE_PRE)
        .append(src)
        .append(START_IMAGE_ALTPRE)
        .append((alt == null) ? "" : alt)
        .append(START_IMAGE_POSTEND);
    return sbuf.toString();
  }//formatImage

  public void appendContent(String str) {
    write(str);
  }//appendContent

  public void appendCode(String str, String type) {
    write(START_CODE);
    write(MarkupLanguageFilter.encodeMLSpecials(str));
    write(END_CODE);
  }//appendCode

  public void appendLineBreak() {
    write(LINEBREAK);
  }//appendLineBReak

  public void startHeader(int i) {
    if (i > 0 && i <= 4) {
      write(START_HEADER_PRE);
      write("" + i);
      write(HEADER_POST);
    }
  }//startHeader

  public void endHeader(int i) {
    if (i > 0 && i <= 4) {
      write(END_HEADER_PRE);
      write("" + i);
      write(HEADER_POST);
    }
  }//startHeader

  public void startTable() {
    write(START_TABLE);
  }//begin_table

  public void endTable() {
    write(END_TABLE);
  }//endTable

  public void startTableCell() {
    write(START_TABLE_CELL);
  }//startCell

  public void endTableCell() {
    write(END_TABLE_CELL);
  }//endCell

  public void startTableRow() {
    write(START_TABLE_ROW);
  }//startRow

  public void endTableRow() {
    write(END_TABLE_ROW);
  }//endRow

  public void startBlockquote() {
    write(START_BLOCKQUOTE);
  }//startBlockquote

  public void endBlockquote() {
    write(END_BLOCKQUOTE);
  }//endBlockquote

  private void write(String str) {
    try {
      m_Out.write(str);
    } catch (IOException ex) {
    }
  }//write

  public static void transform(Reader in, Writer out) throws BBCodeException {
    BBCode2HTML tf = new BBCode2HTML(in, out);
    tf.transform();
  }//transform

  public static void main(String[] args) {
    try {
      StringWriter sw = new StringWriter();
      BBCode2HTML.transform(new FileReader(args[0]), sw);
      sw.flush();
      System.out.println(sw.toString());
    } catch (BBCodeException bbex) {
      System.out.println("BBCode error on line " + bbex.getLine() + "::" + bbex.getMessage());
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//main

  public static final String LINEBREAK = "<br/>";
  public static final String START_BOLD = "<b>";
  public static final String END_BOLD = "</b>";
  public static final String START_ITALIC = "<i>";
  public static final String END_ITALIC = "</i>";
  public static final String START_UNDERLINE = "<u>";
  public static final String END_UNDERLINE = "</u>";

  public static final String START_SIZE_PRE = "<font size=\"";
  public static final String START_SIZE_POST = "\">";
  public static final String END_SIZE = "</font>";

  public static final String START_COLOR_PRE = "<font color=\"";
  public static final String START_COLOR_POST = "\">";
  public static final String END_COLOR = "</font>";

  public static final String START_STYLE_PRE = "<span class=\"";
  public static final String START_STYLE_POST = "\">";
  public static final String END_STYLE = "</span>";

  public static final String START_CODE = "<pre>";
  public static final String END_CODE = "</pre>";

  public static final String START_LINK_PRE = "<a href=\"";
  public static final String START_LINK_POST = "\">";
  public static final String END_LINK = "</a>";

  public static final String START_IMAGE_PRE = "<img src=\"";
  public static final String START_IMAGE_ALTPRE = "\" alt=\"";
  public static final String START_IMAGE_POST = "\">";
  public static final String START_IMAGE_POSTEND = "\"/>";
  public static final String END_IMAGE = "</img>";

  public static final String START_NUMERIC_LIST = "<ol style=\"list-style-type:decimal\">";
  public static final String END_NUMERIC_LIST = "</ol>";

  public static final String START_ALPHANUMERIC_LIST = "<ol style=\"list-style-type:lower-alpha\">";
  public static final String END_ALPHANUMERIC_LIST = "</ol>";

  public static final String START_BULLETED_LIST = "<ul style=\"list-style-type:disc\">";
  public static final String END_BULLETED_LIST = "</ul>";

  public static final String START_LISTITEM = "<li>";
  public static final String END_LISTITEM = "</li>";

  public static final String START_HEADER_PRE = "<h";
  public static final String END_HEADER_PRE = "</h";
  public static final String HEADER_POST = ">";

  public static final String START_TABLE = "<table>";
  public static final String END_TABLE = "</table>";

  public static final String START_TABLE_ROW = "<tr>";
  public static final String END_TABLE_ROW = "</tr>";

  public static final String START_TABLE_CELL = "<td>";
  public static final String END_TABLE_CELL = "</td>";

  public static final String START_BLOCKQUOTE = "<blockquote>";
  public static final String END_BLOCKQUOTE = "</blockquote>";

}//class BBCode2HTML
