/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode.impl;

import net.coalevo.text.model.Transformer;
import net.coalevo.text.model.BaseTransformer;
import net.coalevo.text.model.TransformationException;
import net.coalevo.bbcode.BBCodeTransformer;
import net.coalevo.bbcode.BBCodeException;

/**
 * Implements a {@link Transformer} that transforms
 * from BBCode to plain text (stripping).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BBCode2PlainTransformer
    extends BaseTransformer {

  private BBCodeTransformer m_Transformer;

  public BBCode2PlainTransformer(BBCodeTransformer bbct) {
    super("bbcode","plain");
    m_Transformer = bbct;
  }//constructor

  public String transform(String input)
      throws TransformationException {
    try {
      return m_Transformer.toText(input);
    } catch (BBCodeException e) {
      throw new TransformationException(e.getMessage(),e.getLine());
    }
  }//transform

}//class BBCode2TextTransformer
