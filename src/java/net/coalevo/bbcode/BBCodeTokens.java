/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

/**
 * Provides standard constant BBCode tokens for
 * tags and attributes.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface BBCodeTokens {

  public final static String BR = "br";
  public final static String BOLD = "b";
  public final static String UNDERLINE = "u";
  public final static String ITALIC = "i";
  public final static String IMAGE = "img";
  public final static String URL = "url";
  public final static String LIST ="list";
  public final static String LISTITEM = "li";
  public final static String SIZE="size";
  public final static String CODE = "code";
  public final static String BLOCKQUOTE ="bq";
  public final static String COLOR = "color";
  public final static String STYLE = "style";
  public final static String H1 = "h1";
  public final static String H2 = "h2";
  public final static String H3 = "h3";
  public final static String H4 = "h4";
  public final static String TABLE = "table";
  public final static String TR = "tr";
  public final static String TD = "td";

  public static final String LIST_ALPHANUMERIC = "a";
  public static final String LIST_NUMERIC = "#";
  public static final String LIST_BULLETED = "*";

  public static final String IMAGE_URI_PROTOCOL = "http://";
}//interface BBCodeTokens
