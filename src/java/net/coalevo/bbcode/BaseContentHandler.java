/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Provide a base content handler for BBCode.
 * <p/>
 * Cannot be nested:
 * code, list
 * Cannot have nested tags:
 * url, image
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseContentHandler
    implements BBCodeContentHandler {

  private static Set c_ValidTags;
  private static Set c_ValidListItemTags;
  private static Set c_ValidTableCellTags;
  protected BBCodeEventParser m_Parser;
  protected int m_State = STATE_NORMAL;
  protected int m_WSState = WSSTATE_WHITESPACE;
  protected Stack m_TagStack;
  protected String m_ListType = BBCodeTokens.LIST_BULLETED;
  protected StringBuilder m_CodeSection;
  protected String m_CodeType;
  protected String m_ImageSrc;
  protected StringBuilder m_ImageAlt;
  protected StringBuilder m_LinkText;
  protected String m_Link;

  static {
    c_ValidTags = new HashSet();
    c_ValidTags.add(BBCodeTokens.BOLD);
    c_ValidTags.add(BBCodeTokens.UNDERLINE);
    c_ValidTags.add(BBCodeTokens.ITALIC);
    c_ValidTags.add(BBCodeTokens.COLOR);
    c_ValidTags.add(BBCodeTokens.SIZE);
    c_ValidTags.add(BBCodeTokens.URL);
    c_ValidTags.add(BBCodeTokens.IMAGE);
    c_ValidTags.add(BBCodeTokens.LIST);
    c_ValidTags.add(BBCodeTokens.LISTITEM);
    c_ValidTags.add(BBCodeTokens.STYLE);
    c_ValidTags.add(BBCodeTokens.CODE);
    c_ValidTags.add(BBCodeTokens.BR);
    c_ValidTags.add(BBCodeTokens.H1);
    c_ValidTags.add(BBCodeTokens.H2);
    c_ValidTags.add(BBCodeTokens.H3);
    c_ValidTags.add(BBCodeTokens.H4);
    c_ValidTags.add(BBCodeTokens.TABLE);
    c_ValidTags.add(BBCodeTokens.TR);
    c_ValidTags.add(BBCodeTokens.TD);
    c_ValidTags.add(BBCodeTokens.BLOCKQUOTE);

    c_ValidListItemTags = new HashSet();
    c_ValidListItemTags.add(BBCodeTokens.BOLD);
    c_ValidListItemTags.add(BBCodeTokens.UNDERLINE);
    c_ValidListItemTags.add(BBCodeTokens.ITALIC);
    c_ValidListItemTags.add(BBCodeTokens.COLOR);
    c_ValidListItemTags.add(BBCodeTokens.STYLE);
    c_ValidListItemTags.add(BBCodeTokens.SIZE);
    c_ValidListItemTags.add(BBCodeTokens.BR);
    c_ValidListItemTags.add(BBCodeTokens.URL);
    c_ValidListItemTags.add(BBCodeTokens.LISTITEM);

    c_ValidTableCellTags = new HashSet();
    c_ValidTableCellTags.add(BBCodeTokens.BOLD);
    c_ValidTableCellTags.add(BBCodeTokens.UNDERLINE);
    c_ValidTableCellTags.add(BBCodeTokens.ITALIC);
    c_ValidTableCellTags.add(BBCodeTokens.COLOR);
    c_ValidTableCellTags.add(BBCodeTokens.STYLE);
    c_ValidTableCellTags.add(BBCodeTokens.SIZE);
    c_ValidTableCellTags.add(BBCodeTokens.URL);
    c_ValidTableCellTags.add(BBCodeTokens.TD);

  }//static initializer


  public BaseContentHandler() {
    m_TagStack = new Stack();
  }//BaseContentHandler


  //*** BBCodeContentHandler ***//
  public void startElement(String name, String attr)
      throws BBCodeException {
    //System.out.println("L#="+m_Parser.getCurrentLine() + "::StartTag::State=" + m_State + "::Tag=" + name + "::Attr=" + attr);

    pushTag(name);
    switch (m_State) {
      case STATE_NORMAL:
        if (!isValidTag(name)) {
          throw new BBCodeException("Invalid tag specified " + name, m_Parser.getCurrentLine());
        }
        if (isTopLevel()) {
          if (BBCodeTokens.CODE.equals(name)) {
            m_State = STATE_CODE;
            m_CodeType = attr;
            break;
          } else if (BBCodeTokens.LIST.equals(name)) {
            startList(attr);
            m_State = STATE_LIST;
            break;
          } else if (BBCodeTokens.TABLE.equals(name)) {
            startTable();
            m_State = STATE_TABLE;
            break;
          } else if(BBCodeTokens.BLOCKQUOTE.equals(name)) {
            startBlockquote();
            m_State = STATE_BLOCKQUOTE;
            break;
          }
        }
        if (BBCodeTokens.BOLD.equals(name)) {
          startBold();
        } else if (BBCodeTokens.ITALIC.equals(name)) {
          startItalic();
        } else if (BBCodeTokens.UNDERLINE.equals(name)) {
          startUnderline();
        } else if (BBCodeTokens.SIZE.equals(name)) {
          try {
            startSize(Integer.parseInt(attr));
          } catch (NumberFormatException ex) {
            throw new BBCodeException("Size tag requires an integer attribute.", m_Parser.getCurrentLine());
          }
        } else if (BBCodeTokens.STYLE.equals(name)) {
          startStyle(attr);
        } else if (BBCodeTokens.COLOR.equals(name)) {
          startColor(attr);
        } else if (BBCodeTokens.IMAGE.equals(name)) {
          m_State = STATE_IMG;
          m_ImageSrc = attr;
        } else if (BBCodeTokens.URL.equals(name)) {
          m_State = STATE_URL;
          m_Link = attr;
        } else if (BBCodeTokens.H1.equals(name)) {
          startHeader(1);
        } else if (BBCodeTokens.H2.equals(name)) {
          startHeader(2);
        } else if (BBCodeTokens.H3.equals(name)) {
          startHeader(3);
        } else if (BBCodeTokens.H4.equals(name)) {
          startHeader(4);
        } else if (BBCodeTokens.BR.equals(name)) {
          appendLineBreak();
          popTag();
          m_WSState = WSSTATE_LINEBREAK;
        }
        break;
      case STATE_CODE:
        //add tag to buffer
        throw new BBCodeException("Nested tags not allowed in " + peekTag(), m_Parser.getCurrentLine());
      case STATE_BLOCKQUOTE:
        //add tag to buffer
        throw new BBCodeException("Nested tags not allowed in " + peekTag(), m_Parser.getCurrentLine());
      case STATE_URL:
        if (BBCodeTokens.IMAGE.equals(name)) {
          m_State = STATE_IMG;
          m_ImageSrc = attr;
        }
        break;
      case STATE_IMG:
        //cannot nest tags
        throw new BBCodeException("Nested tags not allowed in " + peekTag(), m_Parser.getCurrentLine());
      case STATE_LIST:
        if (BBCodeTokens.LISTITEM.equals(name)) {
          startListItem();
          m_State = STATE_LISTITEM;
        } else {
          throw new BBCodeException("List needs to be composed of list items.", m_Parser.getCurrentLine());
        }
        break;
      case STATE_LISTITEM:
        if (!c_ValidListItemTags.contains(name)) {
          throw new BBCodeException("Invalid listitem tag specified " + name, m_Parser.getCurrentLine());
        }
        if (BBCodeTokens.LISTITEM.equals(name)) {
          //left open end and start
          endListItem();
          popTag();//need to get the last off.
          startListItem();
          break;
        }
        if (BBCodeTokens.BOLD.equals(name)) {
          startBold();
        } else if (BBCodeTokens.ITALIC.equals(name)) {
          startItalic();
        } else if (BBCodeTokens.UNDERLINE.equals(name)) {
          startUnderline();
        } else if (BBCodeTokens.SIZE.equals(name)) {
          try {
            startSize(Integer.parseInt(attr));
          } catch (NumberFormatException ex) {
            throw new BBCodeException("Size tag requires an integer attribute.", m_Parser.getCurrentLine());
          }
        } else if (BBCodeTokens.STYLE.equals(name)) {
          startStyle(attr);
        } else if (BBCodeTokens.COLOR.equals(name)) {
          startColor(attr);
        } else if (BBCodeTokens.URL.equals(name)) {
          m_State = STATE_URL;
          m_Link = attr;
        } else if (BBCodeTokens.BR.equals(name)) {
          appendLineBreak();
          popTag();
          m_WSState = WSSTATE_LINEBREAK;
        }
        break;
      case STATE_TABLE:
        if (BBCodeTokens.TR.equals(name)) {
          startTableRow();
          m_State = STATE_TABLE_ROW;
        } else {
          throw new BBCodeException("Table needs to be composed of rows.", m_Parser.getCurrentLine());
        }
        break;
      case STATE_TABLE_ROW:
        if (BBCodeTokens.TD.equals(name)) {
          startTableCell();
          m_State = STATE_TABLE_CELL;
        } else {
          throw new BBCodeException("Table row needs to be composed of cells.", m_Parser.getCurrentLine());
        }
        break;
      case STATE_TABLE_CELL:
        if (!c_ValidTableCellTags.contains(name)) {
          throw new BBCodeException("Invalid table cell tag specified " + name, m_Parser.getCurrentLine());
        }
        if (BBCodeTokens.BOLD.equals(name)) {
          startBold();
        } else if (BBCodeTokens.ITALIC.equals(name)) {
          startItalic();
        } else if (BBCodeTokens.UNDERLINE.equals(name)) {
          startUnderline();
        } else if (BBCodeTokens.SIZE.equals(name)) {
          try {
            startSize(Integer.parseInt(attr));
          } catch (NumberFormatException ex) {
            throw new BBCodeException("Size tag requires an integer attribute.", m_Parser.getCurrentLine());
          }
        } else if (BBCodeTokens.STYLE.equals(name)) {
          startStyle(attr);
        } else if (BBCodeTokens.COLOR.equals(name)) {
          startColor(attr);
        } else if (BBCodeTokens.URL.equals(name)) {
          m_State = STATE_URL;
          m_Link = attr;
        }
        break;
    }
    m_WSState = WSSTATE_TAG;
    //System.out.println("STACK=" + m_TagStack);
  }//startElement


  public void newline() throws BBCodeException {
    switch (m_State) {
      case STATE_NORMAL:
      case STATE_LISTITEM:
        if (m_WSState == WSSTATE_CHARACTERS) {
          appendContent(" ");
        }
        break;
      case STATE_BLOCKQUOTE:
        if (m_WSState == WSSTATE_CHARACTERS) {
          appendLineBreak();
        }
    }
    m_WSState = WSSTATE_LINEWRAP;
  }//newline

  public void whitespace(String chars) throws BBCodeException {
    switch (m_State) {
      case STATE_NORMAL:
      case STATE_LISTITEM:
      case STATE_TABLE_CELL:
      case STATE_BLOCKQUOTE:
        appendContent(" ");
        break;
      case STATE_URL:
        if (m_LinkText == null) {
          m_LinkText = new StringBuilder(" ");
        } else {
          m_LinkText.append(" ");
        }
        break;
      case STATE_IMG:
        if (m_ImageAlt == null) {
          m_ImageAlt = new StringBuilder(" ");
        } else {
          m_ImageAlt.append(" ");
        }
        break;
    }
    m_WSState = WSSTATE_WHITESPACE;
  }//whitespace

  public void characters(String chars) throws BBCodeException {
    switch (m_State) {
      case STATE_URL:
        if (m_LinkText == null) {
          m_LinkText = new StringBuilder(chars);
        } else {
          m_LinkText.append(chars);
        }
        break;
      case STATE_IMG:
        if (m_ImageAlt == null) {
          m_ImageAlt = new StringBuilder(chars);
        } else {
          m_ImageAlt.append(chars);
        }
        break;
      case STATE_NORMAL:
      case STATE_LISTITEM:
      case STATE_TABLE_CELL:
      case STATE_BLOCKQUOTE:
        appendContent(chars);
        break;
    }
    m_WSState = WSSTATE_CHARACTERS;
  }//characters

  public void endElement(String name)
      throws BBCodeException {
    //System.out.println("L#="+m_Parser.getCurrentLine() +"::EndTag::State=" + m_State + "::Tag=" + name);
    if (m_TagStack.isEmpty()) {
      throw new BBCodeException(name + " end tag without begin tag.", m_Parser.getCurrentLine());
    }
    String last = peekTag();
    //System.out.println("Name="+name+"::last="+last);

    if (!name.equals(last) && !(name.equals(BBCodeTokens.LIST) && last.equals(BBCodeTokens.LISTITEM))) {
      throw new BBCodeException("Closing tag " + name + " does not match open tag " + last + ".", m_Parser.getCurrentLine());
    }
    popTag();
    //System.out.println("STACK=" + m_TagStack);
    int prevState = peekPreviousState();
    switch (m_State) {
      case STATE_IMG:
        if (!BBCodeTokens.IMAGE.equals(name)) {
          throw new BBCodeException(name + " tag was not closed properly.", m_Parser.getCurrentLine());
        }
        if (prevState == STATE_URL) {
          m_ImageSrc = formatImage(m_ImageSrc, m_ImageAlt.toString());
          m_ImageAlt = null;
        } else {
          appendImage(m_ImageSrc, m_ImageAlt.toString());
          m_ImageSrc = null;
          m_ImageAlt = null;
        }
        m_State = prevState;
        break;
      case STATE_URL:
        if (!BBCodeTokens.URL.equals(name)) {
          throw new BBCodeException(name + "tag was not closed properly.", m_Parser.getCurrentLine());
        }
        //image hyperlinked
        if (m_ImageSrc != null) {
          appendLink(m_Link, m_ImageSrc);
          m_ImageSrc = null;
        } else {
          appendLink((m_Link == null) ? m_LinkText.toString() : m_Link, (m_LinkText == null) ? m_Link : m_LinkText.toString());
        }
        m_LinkText = null;
        m_Link = null;
        m_State = prevState;
        break;
      case STATE_NORMAL:
        if (BBCodeTokens.BOLD.equals(name)) {
          endBold();
        } else if (BBCodeTokens.ITALIC.equals(name)) {
          endItalic();
        } else if (BBCodeTokens.UNDERLINE.equals(name)) {
          endUnderline();
        } else if (BBCodeTokens.SIZE.equals(name)) {
          endSize();
        } else if (BBCodeTokens.STYLE.equals(name)) {
          endStyle();
        } else if (BBCodeTokens.COLOR.equals(name)) {
          endColor();
        } else if (BBCodeTokens.H1.equals(name)) {
          endHeader(1);
        } else if (BBCodeTokens.H2.equals(name)) {
          endHeader(2);
        } else if (BBCodeTokens.H3.equals(name)) {
          endHeader(3);
        } else if (BBCodeTokens.H4.equals(name)) {
          endHeader(4);
        }
        break;
      case STATE_LIST:
        if (BBCodeTokens.LIST.equals(name)) {
          endList();
          m_State = STATE_NORMAL;
        }
        break;
      case STATE_LISTITEM:
        if (BBCodeTokens.LISTITEM.equals(name)) {
          m_State = STATE_LIST;
          endListItem();
        } else if (BBCodeTokens.LIST.equals(name)) {
          //not closed list item
          endListItem();
          endList();
          popTag(); //pop list off, because we popped item off
          m_State = STATE_NORMAL;
        } else {
          if (BBCodeTokens.BOLD.equals(name)) {
            endBold();
          } else if (BBCodeTokens.ITALIC.equals(name)) {
            endItalic();
          } else if (BBCodeTokens.UNDERLINE.equals(name)) {
            endUnderline();
          } else if (BBCodeTokens.SIZE.equals(name)) {
            endSize();
          } else if (BBCodeTokens.STYLE.equals(name)) {
            endStyle();
          } else if (BBCodeTokens.COLOR.equals(name)) {
            endColor();
          }
        }
        break;
      case STATE_TABLE:
        if (BBCodeTokens.TABLE.equals(name)) {
          endTable();
          m_State = STATE_NORMAL;
        }
        break;
      case STATE_TABLE_ROW:
        if (BBCodeTokens.TR.equals(name)) {
          endTableRow();
          m_State = STATE_TABLE;
        }
        break;
      case STATE_TABLE_CELL:
        if (BBCodeTokens.TD.equals(name)) {
          endTableCell();
          m_State = STATE_TABLE_ROW;
        } else {
          if (BBCodeTokens.BOLD.equals(name)) {
            endBold();
          } else if (BBCodeTokens.ITALIC.equals(name)) {
            endItalic();
          } else if (BBCodeTokens.UNDERLINE.equals(name)) {
            endUnderline();
          } else if (BBCodeTokens.SIZE.equals(name)) {
            endSize();
          } else if (BBCodeTokens.STYLE.equals(name)) {
            endStyle();
          } else if (BBCodeTokens.COLOR.equals(name)) {
            endColor();
          }
        }
        break;
      case STATE_BLOCKQUOTE:
        if(BBCodeTokens.BLOCKQUOTE.equals(name)) {
          endBlockquote();
          m_State = STATE_NORMAL;
        }
        break;
    }//switch end
    m_WSState = WSSTATE_ENDTAG;
  }//endElement

  public void preformatted(String chars) throws BBCodeException {
    if (m_State == STATE_CODE) {
      appendCode(chars.toString(), m_CodeType);
      m_CodeType = null;
      popTag(); // get tag off
      m_State = STATE_NORMAL;
    } else {
      appendContent(chars);
    }
  }//preformatted

  //*** BBCodeContentHandler ***//

  //*** Base Content Handling ***//

  protected void startList(String type) {
    //System.out.println("Starting list " + type);
    if (BBCodeTokens.LIST_ALPHANUMERIC.equals(type)) {
      startAlphanumericList();
    } else if (BBCodeTokens.LIST_NUMERIC.equals(type)) {
      startNumericList();
    } else {
      type = BBCodeTokens.LIST_BULLETED;
      startBulletedList();
    }
    m_ListType = type;
  }//startList

  protected void endList() {
    if (BBCodeTokens.LIST_ALPHANUMERIC.equals(m_ListType)) {
      endAlphanumericList();
    } else if (BBCodeTokens.LIST_NUMERIC.equals(m_ListType)) {
      endNumericList();
    } else {
      endBulletedList();
    }
    m_ListType = null;
  }//endList

  public abstract void startBold();

  public abstract void endBold();

  public abstract void startUnderline();

  public abstract void endUnderline();

  public abstract void startItalic();

  public abstract void endItalic();

  public abstract void startColor(String color);

  public abstract void endColor();

  public abstract void startStyle(String syle);

  public abstract void endStyle();

  public abstract void startSize(int size);

  public abstract void endSize();

  public abstract void startAlphanumericList();

  public abstract void endAlphanumericList();

  public abstract void startNumericList();

  public abstract void endNumericList();

  public abstract void startBulletedList();

  public abstract void endBulletedList();

  public abstract void startListItem();

  public abstract void endListItem();

  public abstract void appendLink(String ref, String name);

  public abstract void appendImage(String src, String alt);

  public abstract String formatImage(String src, String alt);

  public abstract void appendContent(String str);

  public abstract void appendCode(String str, String type);

  public abstract void appendLineBreak();

  public abstract void startHeader(int i);

  public abstract void endHeader(int i);

  public abstract void startTable();

  public abstract void endTable();

  public abstract void startTableCell();

  public abstract void endTableCell();

  public abstract void startTableRow();

  public abstract void endTableRow();

  public abstract void startBlockquote();

  public abstract void endBlockquote();

  protected boolean isValidTag(String name) {
    return c_ValidTags.contains(name);
  }//isValidTag

  protected boolean isValidListItemTag(String name) {
    return c_ValidListItemTags.contains(name);
  }//isValidListItemTag

  //*** END Base Content Handling ***//

  //*** Tag Stack Handling ***//

  private boolean isTopLevel() {
    return m_TagStack.size() == 1;
  }//isTopLevel

  private void popTag() {
    m_TagStack.pop();
  }//popTag

  private void pushTag(String name) {
    m_TagStack.push(name);
  }//pushTag

  private String peekTag() {
    return (String) m_TagStack.peek();
  }//peekTag

  private int peekPreviousState() {
    if (m_TagStack.isEmpty()) {
      return STATE_NORMAL;
    } else {
      String name = peekTag();
      if (BBCodeTokens.URL.equals(name)) {
        return STATE_URL;
      } else if (BBCodeTokens.LIST.equals(name)) {
        return STATE_LIST;
      } else if (BBCodeTokens.LISTITEM.equals(name)) {
        return STATE_LISTITEM;
      } else {
        return STATE_NORMAL;
      }
    }
  }//peekPreviousState

  //*** END Tag Stack Handling ***//

  private static final int STATE_NORMAL = 0;

  private static final int STATE_CODE = 3;
  private static final int STATE_URL = 4;
  private static final int STATE_IMG = 5;
  private static final int STATE_LIST = 6;
  private static final int STATE_LISTITEM = 7;
  private static final int STATE_BOLD = 8;
  private static final int STATE_TABLE = 9;
  private static final int STATE_TABLE_ROW = 10;
  private static final int STATE_TABLE_CELL = 11;
  private static final int STATE_BLOCKQUOTE = 12;

  private static final int WSSTATE_WHITESPACE = 0;
  private static final int WSSTATE_LINEBREAK = 1;
  private static final int WSSTATE_LINEWRAP = 2;
  private static final int WSSTATE_CHARACTERS = 3;
  private static final int WSSTATE_TAG = 4;
  private static final int WSSTATE_ENDTAG = 5;

}//class BaseContentHandler
