header {
package net.coalevo.bbcode;

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
}

{
/**
 * Provides a generic BBCode Lexer that calls 
 * the provided {@link BBCodeContentHandler} for
 * handling (event style parser).
 * <p>
 * This lexer is basically used for parsing, 
 * in a way similar to a SAX Parser,
 * because it will pass events to its associated
 * {@link BBCodeContentHandler} instance, which 
 * has to handle the actual document or fragment 
 * structure.
 * </p>
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 * @see BBCodeContentHandler
 */
}
class BBCodeEventLexer extends Lexer;
options {
    classHeaderPrefix="";
	k = 3;
	charVocabulary = '\3'..'\377';
	caseSensitive=false;
	defaultErrorHandler=false;
}

//Handler code
{

  private BBCodeContentHandler m_Handler;
  private LineCounter m_Line;

  /**
   * Constructs a new <tt>BBCodeEventLexer</tt> instance
   * with an associated {@link BBCodeContentHandler} and
   * a <tt>java.io.Reader</tt> as source to read from.
   *
   * @param r the <tt>java.io.Reader</tt> providing the content to
   *          be parsed.
   * @param handler a {@link BBCodeContentHandler} instance.
   */
  public BBCodeEventLexer(Reader r, BBCodeContentHandler handler, LineCounter lc) {
    this(r);
    m_Handler = handler;
    m_Line = lc;
  }//constructor
}

STARTELEMENT
: LB w:WORD (a:ATTR)? RB {
   m_Handler.startElement(w.getText(),((a==null)? null: a.getText()));
   $setType(STARTELEMENT);
};


ENDELEMENT
: LB SL w:WORD RB {
  m_Handler.endElement(w.getText());
  $setType(ENDELEMENT);
};

NEWLINE
    options {
       generateAmbigWarnings=false;
    }
    : ('\r' '\n')! {newline(); m_Line.increment(); m_Handler.newline();$setType(NEWLINE);}
    | '\r'!		   {newline(); m_Line.increment(); m_Handler.newline();$setType(NEWLINE);}
	| '\n'!        {newline(); m_Line.increment(); m_Handler.newline();$setType(NEWLINE);}
	;
    
WHITESPACE
: (' ' |'\t')+ 
{ 
  m_Handler.whitespace(getText());
  $setType(WHITESPACE);
};

CHARACTERS
: (CHARACTER)+ 
{ m_Handler.characters(getText());
 $setType(CHARACTERS);
};

PRE
: "[["!
    (               /* '\r' '\n' can be matched in one alternative or by matching
                       '\r' in one iteration and '\n' in another. I am trying to
                       handle any flavor of newline that comes in, but the language
                       that allows both "\r\n" and "\r" and "\n" to all be valid
                       newline is ambiguous. Consequently, the resulting grammar
                       must be ambiguous. I'm shutting this warning off.
                    */
      options {
        generateAmbigWarnings=false;
        greedy=false;
      }
      : '\r' '\n' {newline();m_Line.increment();}
      | '\r'      {newline();m_Line.increment();}
      | '\n'      {newline();m_Line.increment();}
      | ~('\n'|'\r')
    )*
    "]]"!
    {
      m_Handler.preformatted(getText());
      $setType(PRE);
    }
;

protected EQ: '=';
protected LB: '[';
protected RB: ']';
protected SL: '/';

protected ATTR: EQ! (INT|COLOR|LISTTYPE|WORD|URL);

protected WORD: SCHAR ('a'..'z'|'0'..'9')*;
protected INT: ('0'..'9')+;
protected SCHAR: ('a'..'z');
protected COLOR: ('#') HEX HEX HEX;
protected HEX: ('0'..'9'|'a'..'f') ('0'..'9'|'a'..'f');
protected LISTTYPE: ('#'|'*');
protected SPACE: (' ')+;
protected NOTCHARACTER: '['|'\n'|'\r'|'\t'|']'|' ';
protected CHARACTER: ~('['|'\n'|'\r'|'\t'|']'|' ');
protected URL: '"'! WORD ':' (URLSTUFF)+ '"'!;
protected URLSTUFF
   :  ('-' | '_' | '.' | '!' | '~' | '*' | '\'' | '(' | ')')
   | (';' | '?' | ':' | '@' | '&' | '=' | '+' | '$' | ',' | '/' | '#')
   | ('a'..'z' | '0'..'9')
   ;
   
{
import java.io.Reader;

/**
 * Provides a generic BBCodeEventParser, that
 * uses a {@link BBCodeEventLexer} instance for
 * most of the work.
 * <p>
 * This parser is similar to a SAX Parser,
 * because it will pass events to its associated
 * {@link BBCodeContentHandler} instance, which 
 * has to handle the actual document or fragment 
 * structure.
 * </p>
 * <p>
 * The method that will parse the document and generate
 * content events is <tt>parse()</tt>.
 * </p>
 * 
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 * @see BBCodeContentHandler
 */
}
class BBCodeEventParser extends Parser;
options {	
  //classHeaderPrefix="";
  k=1;
}

//constructor
{

  private LineCounter m_Line;

  /**
   * Constructs a new <tt>BBCodeEventParser</tt> instance
   * with an associated {@link BBCodeContentHandler} and
   * a <tt>java.io.Reader</tt> as source to read from.
   *
   * @param r the <tt>java.io.Reader</tt> providing the content to
   *          be parsed.
   * @param handler a {@link BBCodeContentHandler} instance.
   */
  private BBCodeEventParser(Reader r, BBCodeContentHandler handler,LineCounter lc) {
    this(new BBCodeEventLexer(r,handler,lc));
    m_Line = lc;
  }//constructor

  /**
   * Returns the current line number.
   *
   * @return the current line number as <tt>int</tt>.
   */
  public int getCurrentLine() {
    return m_Line.getLine();
  }//getLine

  /**
   * Factory method for a new <tt>BBCodeEventParser</tt> instance
   * with an associated {@link BBCodeContentHandler} and
   * a <tt>java.io.Reader</tt> as source to read from.
   *
   * @param r the <tt>java.io.Reader</tt> providing the content to
   *          be parsed.
   * @param handler a {@link BBCodeContentHandler} instance.
   */
  public static BBCodeEventParser create(Reader r, BBCodeContentHandler h) {
    LineCounter line = new LineCounter();
    return new BBCodeEventParser(r,h,line);
  }//createBBCodeEventParser

}

parse
throws BBCodeException
: t:(STARTELEMENT| PRE |CHARACTERS | NEWLINE | WHITESPACE | ENDELEMENT)*;

