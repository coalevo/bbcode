/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bbcode;

import java.io.*;

public class Tool {

  public static String version = "1.0";

  public static void main(String[] args) throws Exception {
    try {

      if (args == null || args.length == 0) {
        printUsage();
        System.exit(-1);
      }

      String infile = "stdin";
      String outfile = "stdout";
      String target = "html";

      for (int i = 0; i < args.length; i++) {
        String arg = args[i];
        //Targets
        if (arg.startsWith("-target=")) {
          String a = arg.substring(8);
          if ("ansi".equals(a)) {
            target = "ansi";
            continue;
          }
          if ("html".equals(a)) {
            target = "html";
            continue;
          }
          if ("telnetd".equals(a)) {
            target = "telnetd";
            continue;
          }
          if ("text".equals(a)) {
            target = "text";
            continue;
          }
          printUsage();
          System.exit(-1);
        }
        //Input and Output
        if (arg.startsWith("-in=")) {
          infile = arg.substring(4);
        }
        if (arg.startsWith("-out=")) {
          outfile = arg.substring(5);
        }
      }

      //3. Input
      Reader r = null;
      if ("stdin".equals(infile)) {
        r = new InputStreamReader(System.in);
      } else {
        r = new FileReader(infile);
      }

      //4. Output
      Writer w = null;
      if ("stdout".equals(outfile)) {
        w = new PrintWriter(System.out);
      } else {
        w = new FileWriter(outfile);
      }
      if ("ansi".equals(target)) {
        BBCode2ANSI.transform(r, w);
      }
      if ("html".equals(target)) {
        BBCode2HTML.transform(r, w);
      }
      if ("telnetd".equals(target)) {
        BBCode2Telnetd.transform(r, w);
      }
      if ("text".equals(target)) {
        BBCode2Text.transform(r,w);
      }

      w.flush();
      w.close();
      r.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//main

  public static void printMessage() {
    System.err.println("BBCode Version " + version + "(c) 2005-2007 Dieter Wimberger");
  }//printMessage

  public static void printUsage() {
    System.err.println("Usage: java net.coalevo.bbcode.Tool [args]");
    System.err.println("  -in=               input filename (stdin for console; default).");
    System.err.println("  -out=              output filename (stdout for console; default).");
    System.err.println("  -target=name       output target (default is html).");
    System.err.println();
    System.err.println("Targets:");
    System.err.println("html     transform to HTML fragment.");
    System.err.println("ansi     transform to ANSI text (e.g. with colors).");
    System.err.println("telnetd  transform to TelnetD markup.");
    System.err.println("text     transform to plain text (stripped completely).");
    System.err.println();
  }//printUsage

}//Tool
